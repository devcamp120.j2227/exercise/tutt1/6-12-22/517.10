import './App.css';
import Info from './components/componentInfo';

function App() {
  return (
    <div>
      <Info firstName="Vu Dang" lastName="Minh" numberFavourite={25}/>
    </div>
  );
}

export default App;