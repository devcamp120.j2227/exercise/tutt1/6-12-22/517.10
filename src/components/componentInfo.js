const { Component } = require("react");

class Info extends Component {
    render() {
        let { firstName, lastName, numberFavourite } = this.props
        return (
            <div>
                <p>My name is {firstName} {lastName} and my favourite number is {numberFavourite}.</p>
            </div>
        )
    }
}

export default Info